# encoding: utf-8

from __future__ import unicode_literals

from datetime import date

from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from flow.models import FSA

from .models import DMPSchema
from .fields import DateRangeField


class PlanForm(forms.ModelForm):
    plan_type = forms.ModelChoiceField(queryset=FSA.objects.all())

    class Meta:
        model = DMPSchema
        fields = ['plan_name']

    def __init__(self, *args, **kwargs):
        super(PlanForm, self).__init__(*args, **kwargs)

        # crispy forms
        self.helper = FormHelper()
        self.helper.form_id = 'id-plan'
        self.helper.form_class = 'blueForms'
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Next'))

class DeleteForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(DeleteForm, self).__init__(*args, **kwargs)

        # crispy forms
        self.helper = FormHelper()
        self.helper.form_id = 'id-plan'
        self.helper.form_class = 'blueForms'
        self.helper.form_method = 'post'
        self.helper.add_input(Submit('submit', 'Yes, really delete'))


class AbstractStateForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.state = kwargs.pop('state')
        self.label = kwargs.pop('label')
        self.help_text = getattr(self.state, 'help_text', '')
        self.choices = kwargs.pop('choices', None)
        self.has_prevstate = kwargs.pop('has_prevstate', False)
        kwargs.pop('instance', None)
        super(AbstractStateForm, self).__init__(*args, **kwargs)

        self._add_state_field()
        self._add_notes_field()

        # crispy forms
        self.helper = FormHelper()
        self.helper.form_id = 'id-{}'.format(self.state.name)
        self.helper.form_class = 'blueForms'
        self.helper.form_method = 'post'
        if self.has_prevstate:
            self.helper.add_input(Submit('prev', 'Prev'))
        self.helper.add_input(Submit('submit', 'Next'))

    def _add_state_field(self):
        pass

    def _add_notes_field(self):
        notes = forms.CharField(required=False, widget=forms.Textarea)
        self.fields['notes'] = notes

    def serialize(self):
        return {
            'state': self.cleaned_data['state'],
            'notes': self.cleaned_data.get('notes', ''),
        }


class BooleanForm(AbstractStateForm):

    def _add_state_field(self):
        choices = (
            (True, 'Yes.'),
            (False, 'No.'),
        )
        self.fields['state'] = forms.ChoiceField(
            label=self.label,
            help_text=self.help_text,
            choices=choices,
            widget=forms.RadioSelect,
        )

    def pprint(self):
        if self.is_valid():
            return 'Yes' if self.cleaned_data['state'] else 'No'
        return 'Not set'

    def serialize(self):
        out = {
            'notes': self.cleaned_data.get('notes', ''),
            'state': None,
        }
        if self.is_valid():
            data = self.cleaned_data['state']
            if data == 'True':
                out['state'] = True
            if data == 'False':
                out['state'] = False
            return out
        return {}

class ChoiceForm(AbstractStateForm):

    def _add_state_field(self):
        choices = [(k, k) for k in self.choices]
        self.fields['state'] = forms.ChoiceField(
            label=self.label,
            help_text=self.help_text,
            choices=choices,
            widget=forms.RadioSelect,
        )

    def pprint(self):
        if self.is_valid():
            return self.cleaned_data['state']
        return 'Not set'


class MultipleChoiceOneTextForm(AbstractStateForm):

    def _add_state_field(self):
        choices = self.choices.items()
        self.fields['state'] = forms.MultipleChoiceField(
            label=self.label,
            help_text=self.help_text,
            choices=choices,
            widget=forms.CheckboxSelectMultiple,
        )

    def pprint(self):
        if self.is_valid():
            return self.cleaned_data['state']
        return 'Not set'


class DateRangeForm(AbstractStateForm):

    def __init__(self, *args, **kwargs):
        initial = kwargs.pop('initial', {})
        state = initial.pop('state', {})
        if state:
            start = state.pop('start')
            end = state.pop('end')
            state = {}
            state['lower'] = date(*map(int, start.split('-')))
            state['upper'] = date(*map(int, end.split('-')))
        initial['state'] = state
        kwargs['initial'] = initial
        super(DateRangeForm, self).__init__(*args, **kwargs)

    def _add_state_field(self):
        self.fields['state'] = DateRangeField(
            label=self.label,
            help_text=self.help_text,
        )

    def serialize(self):
        if self.is_bound:
            data = self.cleaned_data['state']
            assert data, "Dateranges may not be empty"
            start, end = data.lower, data.upper
            return {
                'state':
                    {
                        'start': start.isoformat(),
                        'end': end.isoformat(),
                    },
                'notes': self.cleaned_data.get('notes', ''),
            }
        return {}

    def pprint(self):
        if self.is_valid():
            return '{}–{}'.format(self.serialize())
        return 'Not set'


def make_form(state, **kwargs):
    kwargs.pop('prefix', None)
    kwargs.pop('instance', None)
    choices = kwargs.get('choices', None)
    answerdict = {
        'state': state,
        'prefix': state.name,
        'label': state.question,
    }
    kwargs.update(answerdict)
    if state.input_type == 'bool':
        form_type = BooleanForm
    elif state.input_type == 'choice' and choices:
        form_type = ChoiceForm
        kwargs['choices'] = choices
    elif state.input_type == 'multichoiceonetext' and choices:
        form_type = MultipleChoiceOneTextForm
        kwargs['choices'] = choices
    elif state.input_type == 'daterange':
        form_type = DateRangeForm
    else:
        assert False, 'Unknown input type: {}'.format(state.input_type)
    return form_type(**kwargs)
