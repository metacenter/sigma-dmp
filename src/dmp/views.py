from collections import OrderedDict
from itertools import chain

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import (
    FormView, CreateView, UpdateView, DetailView, ListView, DeleteView)

from .models import DMPSchema
from .forms import make_form, PlanForm, DeleteForm
from flow.models import FSA


def progress(so_far, all):
    "Returns percentage done, as float"
    return so_far/float(all)*100


class DeleteFormMixin(FormView):
    form_class = DeleteForm


class NewPlanView(LoginRequiredMixin, CreateView):
    template_name = 'dmp/newplan_form.html'
    model = DMPSchema
    form_class = PlanForm

    def get_success_url(self):
        kwargs = {
            'plan': self.object.pk,
            'state': self.object.fsa.start.name,
        }
        return reverse('new_state', kwargs=kwargs)

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.flowdata = {}
        self.object.previous_flowdata = {}
        self.object.added_by = self.request.user
        self.object.fsa = form.cleaned_data['plan_type']
        self.object.save()
        return super(NewPlanView, self).form_valid(form)


class DeletePlanView(LoginRequiredMixin, DeleteFormMixin, DeleteView):
    model = DMPSchema
    template_name = 'dmp/plan_confirm_delete.html'
    success_url = reverse_lazy('plan_list')
    pk_url_kwarg = 'plan'

    def get_queryset(self):
        qs = super(DeletePlanView, self).get_queryset()
        return qs.filter(added_by=self.request.user)


class AbstractStateMixin(object):

    def get_statename(self):
        statename = self.kwargs.get('state')
        assert statename in self.get_fsa().statemap, "Unknown state: %s" % statename
        return statename

    def get_state(self):
        statename = self.get_statename()
        return self.get_fsa().statemap[statename]

    def get_plan(self):
        return self.kwargs.get('plan')

    def get_queryset(self):
        return self.model.objects.filter(pk=self.get_plan())

    def get_fsa(self):
        plan_id = self.get_plan()
        dmpschema = DMPSchema.objects.get(id=plan_id)
        if dmpschema.fsa:
            return dmpschema.fsa
        return FSA.objects.get(id=1)


class AbstractStateView(LoginRequiredMixin, AbstractStateMixin, UpdateView):
    model = DMPSchema
    template_name = 'dmp/state_form.html'
    pk_url_kwarg = 'plan'
    fields = ('plan_name', 'flowdata')


class NewStateView(AbstractStateView):

    def get_initial(self):
        current_data = self.object.flowdata or {}
        previous_data = self.object.previous_flowdata or {}
        statename = self.get_statename()
        initial = current_data.get(statename, {})
        if not initial:
            initial = previous_data.get(statename, {})
        return initial

    def get_success_url(self):
        statename = self.get_statename()
        current_data = self.object.flowdata
        fsa = self.get_fsa()
        nextstate = fsa.nextstate(statename, current_data)
        kwargs = {'plan': self.object.pk}
        if nextstate:
            kwargs['state'] = nextstate
            return reverse('new_state', kwargs=kwargs)
        return reverse('plan_detail', kwargs=kwargs)

    def get_context_data(self, **kwargs):
        fsa = self.get_fsa()
        state = self.get_state()
        kwargs['question'] = state.question
        kwargs['statename'] = state.name
        kwargs['answers'] = state.texts.values()
        kwargs['framing_text'] = state.framing_text
        sections = fsa.get_states_per_section()
        section = state.section
        kwargs['section'] = section
        current_states_for_section = sections[section.name]
        before_me = True
        for i, statename in enumerate(current_states_for_section):
            if statename == state.name:
                before_me = False
            stateinfo = {
                'statename': statename,
                'show': before_me,
                'self': statename == state.name,
            }
            current_states_for_section[i] = stateinfo
        kwargs['states_in_section'] = current_states_for_section
        num_states = fsa.states.count()
        states_so_far = fsa.get_maximal_previous_states(state.name)
        kwargs['progress'] = progress(len(states_so_far), num_states)
        return super(NewStateView, self).get_context_data(**kwargs)

    def get_form(self, **_):
        form_kwargs = self.get_form_kwargs()
        state = self.get_state()
        choices = []
        if state.input_type == 'choice':
            choices = state.texts.values_list('condition', 'text')
        if state.input_type == 'multichoiceonetext':
            choices = state.texts.values_list('condition', 'condition')
        generate_kwargs = {
            'choices': dict(choices),
            'has_prevstate': state.get_prev_state(self.object.flowdata)
        }
        generate_kwargs.update(form_kwargs)
        form = make_form(state, **generate_kwargs)
        return form

    def delete_statedata(self, *args):
        """Delete data for the statenames in args"""
        for statename in args:
            self.object.flowdata.pop(statename, None)
        self.object.save()

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if 'prev' in request.POST:
            prev_state = self.get_state().get_prev_state(self.object.flowdata)
            kwargs = {
                'state': prev_state.name,
                'plan': self.object.pk,
            }
            return HttpResponseRedirect(reverse('new_state', kwargs=kwargs))
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        state_switch = form.serialize()
        statename = self.get_statename()
        # save change
        current_data = self.object.flowdata
        current_data[statename] = state_switch
        self.object.flowdata = current_data
        previous_data = self.object.previous_flowdata
        previous_data[statename] = state_switch
        self.object.previous_flowdata = previous_data
        self.object.save()
        # remove invalidated states
        paths_from = self.get_fsa().find_paths_from(statename)
        invalidated_states = set(chain(*paths_from))
        invalidated_states.discard(None)
        invalidated_states.discard(statename)
        self.delete_statedata(*invalidated_states)
        return HttpResponseRedirect(self.get_success_url())


class PlanListView(LoginRequiredMixin, ListView):
    model = DMPSchema
    template_name = 'dmp/plan_list.html'

    def get_queryset(self):
        return self.model.objects.filter(added_by=self.request.user).order_by('-added')


class AbstractPlanDetailView(LoginRequiredMixin, AbstractStateMixin, DetailView):
    model = DMPSchema
    pk_url_kwarg = 'plan'

    def get_text(self):
        data = self.object.flowdata.copy()
        return self.get_fsa().generate_text(data)

    def massage_text(self, text):
        return text

    def get_context_data(self, **kwargs):
        kwargs['data'] = self.object.flowdata.copy()
        kwargs['text'] = self.get_text()
        return super(AbstractPlanDetailView, self).get_context_data(**kwargs)


class PlanDetailView(AbstractPlanDetailView):
    template_name = 'dmp/plan_detail.html'

    def get_context_data(self, **kwargs):
        kwargs = super(PlanDetailView, self).get_context_data(**kwargs)
        outputs = OrderedDict()
        data = kwargs['data']
        fsa = self.get_fsa()
        for statename, stateclass in fsa.statemap.items():
            value = data.get(statename, None)
            if not value or value['state'] is None:
                continue
            if stateclass.input_type == 'bool':
                value['state'] = 'Yes' if value['state'] else 'No'
            elif stateclass.input_type == 'multilist':
                value['state'] = ', '.join(value['state'])
            elif stateclass.input_type == 'daterange':
                value['state'] = '[{start}, {end}]'.format(**value['state'])
            outputs[statename] = value
        kwargs['scores'], _ = fsa.get_scores(data)
        kwargs['output'] = OrderedDict(fsa.order_data(outputs))
        return kwargs


class GeneratedPlanHTMLView(AbstractPlanDetailView):
    template_name = 'dmp/generated_plan.html'


class GeneratedPlanPlainTextView(AbstractPlanDetailView):
    template_name = 'dmp/generated_plan.txt'
    content_type = 'text/plain; charset=UTF-8'


class GeneratedPlanPDFView(AbstractPlanDetailView):
    template_name = 'dmp/generated_plan.pdf'
    content_type = 'text/plain'
