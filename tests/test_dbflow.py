# encoding: utf-8

from __future__ import unicode_literals

from django import test
from datetime import date
from collections import OrderedDict

from flow.models import Section, Text, State, Score
from flow.models import BooleanState, ChoiceState, DateRangeState
from flow.models import MultipleChoiceOneTextState
from flow.models import StateRelation
from flow.models import FSA


def generate_states(start, **canned_data):
    s1 = BooleanState.objects.create(name='S1', position=2, **canned_data)
    s2 = ChoiceState.objects.create(name='S2', position=3, **canned_data)
    s3 = ChoiceState.objects.create(name='S3', position=4, **canned_data)
    s4 = ChoiceState.objects.create(name='S4', position=5, **canned_data)
    s5 = ChoiceState.objects.create(name='S5', position=6, **canned_data)
    states = {
        'start': start,
        's1': s1,
        's2': s2,
        's3': s3,
        's4': s4,
        's5': s5,
    }
    srstart1 = StateRelation.objects.create(prev_state=start, next_state=s1)
    sr12 = StateRelation.objects.create(condition=True, prev_state=s1, next_state=s2)
    sr13 = StateRelation.objects.create(condition=False, prev_state=s1, next_state=s3)
    sr24 = StateRelation.objects.create(prev_state=s2, next_state=s4)
    sr34 = StateRelation.objects.create(prev_state=s3, next_state=s4)
    sr45 = StateRelation.objects.create(prev_state=s4, next_state=s5)
    sr5end = StateRelation.objects.create(prev_state=s5)
    return states


class CannedData(object):

    def setUp(self):
        self.fsa = FSA.objects.create(name='FSA')
        self.section = Section.objects.create(
            fsa=self.fsa,
            name='Miscellaneous',
        )
        self.canned_data = {
            'section': self.section,
            'fsa': self.fsa,
            'question': 'hdg',
        }

class TestStatePolymorphism(CannedData, test.TestCase):

    def test_save_as_bool(self):
        s = State.objects.create(
            name='S',
            input_type='bool',
            **self.canned_data
        )
        self.assertEqual(s.__class__, BooleanState)

    def test_save_as_choice(self):
        s = State.objects.create(
            name='S',
            input_type='choice',
            **self.canned_data
        )
        self.assertEqual(s.__class__, ChoiceState)

    def test_save_as_daterange(self):
        s = State.objects.create(
            name='S',
            input_type='daterange',
            **self.canned_data
        )
        self.assertEqual(s.__class__, DateRangeState)

    def test_save_as_multichoiceonetext(self):
        s = State.objects.create(
            name='S',
            input_type='multichoiceonetext',
            **self.canned_data
        )
        self.assertEqual(s.__class__, MultipleChoiceOneTextState)

    def test_change_from_choice_to_bool(self):
        s = State.objects.create(
            name='S',
            input_type='choice',
            **self.canned_data
        )
        oldclass = s.__class__
        s.input_type = 'bool'
        s.save()
        self.assertNotEqual(oldclass, s.__class__)
        self.assertEqual(s.__class__, BooleanState)


class TestStateGetTextMethods(CannedData, test.TestCase):

    def test_extra_fields_get_text(self):
        s = BooleanState.objects.create(name='S', **self.canned_data)
        expected = {'bar': 'bar', 'foo': 'foo', 'text': ''}
        result = s.get_text(True, foo='foo', bar='bar')
        self.assertEqual(result, expected)

    def test_bool_no_text_get_text(self):
        s = BooleanState.objects.create(name='S', **self.canned_data)
        result = s.get_text(True)
        self.assertEqual(result['text'], '')

    def test_bool_one_text_get_text(self):
        s = BooleanState.objects.create(name='S', **self.canned_data)
        expected = 'ABC 123'
        t_TRUE = Text.objects.create(
            state=s,
            text=expected,
        )
        result_TRUE = s.get_text(True)
        self.assertEqual(result_TRUE['text'], expected)
        result_whatever = s.get_text(False)
        self.assertEqual(result_whatever['text'], expected)

    def test_bool_get_text(self):
        s = BooleanState.objects.create(name='S', **self.canned_data)
        t_TRUE_expected = 'ABC 123'
        t_TRUE = Text.objects.create(
            state=s,
            condition='TRUE',
            text=t_TRUE_expected,
        )
        t_FALSE_expected = 'DEF 456'
        t_FALSE = Text.objects.create(
            state=s,
            condition='FALSE',
            text=t_FALSE_expected,
        )
        self.assertEqual(set((t_TRUE, t_FALSE)), set(s.texts.all()))
        result_TRUE = s.get_text(True)
        self.assertEqual(result_TRUE['text'], t_TRUE_expected)
        result_FALSE = s.get_text(False)
        self.assertEqual(result_FALSE['text'], t_FALSE_expected)
        self.assertNotEqual(result_TRUE, result_FALSE)

    def test_choice_get_text(self):
        s = ChoiceState.objects.create(name='S', **self.canned_data)
        t_A_expected = 'ABC 123'
        t_A = Text.objects.create(
            state=s,
            condition='A',
            text=t_A_expected,
        )
        t_B_expected = 'DEF 456'
        t_B = Text.objects.create(
            state=s,
            condition='B',
            text=t_B_expected,
        )
        t_C_expected = 'GHI 789'
        t_C = Text.objects.create(
            state=s,
            condition='C',
            text=t_C_expected,
        )
        self.assertEqual(set((t_A, t_B, t_C)), set(s.texts.all()))
        result_A = s.get_text('A')
        self.assertEqual(result_A['text'], t_A_expected)
        result_B = s.get_text('B')
        self.assertEqual(result_B['text'], t_B_expected)
        result_C = s.get_text('C')
        self.assertEqual(result_C['text'], t_C_expected)
        self.assertNotEqual(result_A, result_B)
        self.assertNotEqual(result_A, result_C)
        self.assertNotEqual(result_B, result_C)

    def test_multichoiceonetext_get_text(self):
        s = MultipleChoiceOneTextState.objects.create(name='S', **self.canned_data)
        t_A_expected = 'ABC 123'
        t_A = Text.objects.create(
            state=s,
            condition='A',
            text=t_A_expected,
        )
        t_B_expected = 'DEF 456'
        t_B = Text.objects.create(
            state=s,
            condition='B',
            text=t_B_expected,
        )
        t_C_expected = 'GHI 789'
        t_C = Text.objects.create(
            state=s,
            condition='C',
            text=t_C_expected,
        )
        self.assertEqual(set((t_A, t_B, t_C)), set(s.texts.all()))
        result_A = s.get_text('A')
        self.assertEqual(result_A['text'], 'A')
        result_many = s.get_text(['B', 'C'])
        expected = '{} and {}'.format('B', 'C')
        self.assertEqual(result_many['text'], expected)

    def test_daterange_get_text(self):
        daterange = {'start': date(1980, 1, 1), 'end': date(1990, 1, 1)}
        text = 'From {start} to {end}'
        s = DateRangeState.objects.create(
            name='S',
            framing_text=text,
            **self.canned_data
        )
        t = Text.objects.create(
            state=s,
            text=text,
        )
        expected = text.format(**daterange)
        result = s.get_text(daterange)
        self.assertEqual(result['text'], expected)


class TestStateNextstateMethods(CannedData, test.TestCase):

    def test_no_nextstate(self):
        s = DateRangeState.objects.create(name='S1', **self.canned_data)
        result = s.get_next_state(None)
        self.assertEqual(result, None)

    def test_default_nextstate(self):
        s1 = DateRangeState.objects.create(name='S1', **self.canned_data)
        s2 = ChoiceState.objects.create(name='S2', **self.canned_data)
        sr = StateRelation.objects.create(prev_state=s1, next_state=s2)
        result = s1.get_next_state(None)
        self.assertEqual(result, s2)

    def test_branching_nextstate(self):
        s1 = BooleanState.objects.create(name='S1', **self.canned_data)
        s2 = ChoiceState.objects.create(name='S2', **self.canned_data)
        s3 = ChoiceState.objects.create(name='S3', **self.canned_data)
        sr12 = StateRelation.objects.create(condition=True, prev_state=s1, next_state=s2)
        sr13 = StateRelation.objects.create(condition=False, prev_state=s1, next_state=s3)
        with self.assertRaises(StateRelation.DoesNotExist):
            result_None = s1.get_next_state({'S1': {'state': None}})
        result_True = s1.get_next_state({'S1': {'state': True}})
        self.assertEqual(result_True, s2)
        result_False = s1.get_next_state({'S1': {'state': False}})
        self.assertEqual(result_False, s3)

    def test_branching_depends_nextstate(self):
        s0 = BooleanState.objects.create(name='S0', **self.canned_data)
        s1 = BooleanState.objects.create(name='S1', depends=s0, **self.canned_data)
        s2 = ChoiceState.objects.create(name='S2', **self.canned_data)
        s3 = ChoiceState.objects.create(name='S3', **self.canned_data)
        sr12 = StateRelation.objects.create(condition=True, prev_state=s1, next_state=s2)
        sr13 = StateRelation.objects.create(condition=False, prev_state=s1, next_state=s3)
        with self.assertRaises(StateRelation.DoesNotExist):
            result_None = s1.get_next_state({'S0': {'state': None}})
        result_True = s1.get_next_state({'S0': {'state': True}})
        self.assertEqual(result_True, s2)
        result_False = s1.get_next_state({'S0': {'state': False}})
        self.assertEqual(result_False, s3)


class TestStatePrevStateTestCase(CannedData, test.TestCase):

    def test_no_prevstate(self):
        s = DateRangeState.objects.create(name='S1', **self.canned_data)
        result = s.get_prev_state(None)
        self.assertEqual(result, None)
        fsa = FSA.objects.create(name='Nada')
        fsa.start = s
        fsa.save()
        sr = StateRelation.objects.create(next_state=s)
        result = s.get_prev_state(None)
        self.assertEqual(result, None)

    def test_single_prevstate(self):
        s1 = DateRangeState.objects.create(name='S1', **self.canned_data)
        s2 = ChoiceState.objects.create(name='S2', **self.canned_data)
        sr = StateRelation.objects.create(prev_state=s1, next_state=s2)
        result = s2.get_prev_state(None)
        self.assertEqual(result, s1)

    def test_xor_multiple_prevstate(self):
        """
        s1 -> s2
        s1 -> s3
        s2 -> s4
        s3 -> s4
        """
        s1 = BooleanState.objects.create(name='S1', **self.canned_data)
        s2 = ChoiceState.objects.create(name='S2', **self.canned_data)
        s3 = ChoiceState.objects.create(name='S3', **self.canned_data)
        s4 = ChoiceState.objects.create(name='S4', **self.canned_data)
        sr1 = StateRelation.objects.create(next_state=s1)
        sr12 = StateRelation.objects.create(condition=True, prev_state=s1, next_state=s2)
        sr13 = StateRelation.objects.create(condition=False, prev_state=s1, next_state=s3)
        sr24 = StateRelation.objects.create(condition=False, prev_state=s2, next_state=s4)
        sr34 = StateRelation.objects.create(condition=False, prev_state=s3, next_state=s4)
        sr4end = StateRelation.objects.create(condition=False, prev_state=s4)
        self.fsa.start = s1
        self.fsa.save()
        result_True = s4.get_prev_state({'S1': {'state': True}, 'S2': {'state': None}})
        self.assertEqual(result_True, s2)
        result_True = s4.get_prev_state({'S1': {'state': False}, 'S3': {'state': None}})
        self.assertEqual(result_True, s3)

    def test_shortcut_multiple_prevstate(self):
        """
        s1 -> s2
        s1 -> s3
        s2 -> s3
        """
        s1 = BooleanState.objects.create(name='S1', **self.canned_data)
        s2 = ChoiceState.objects.create(name='S2', **self.canned_data)
        s3 = ChoiceState.objects.create(name='S3', **self.canned_data)
        sr1 = StateRelation.objects.create(next_state=s1)
        sr12 = StateRelation.objects.create(condition=True, prev_state=s1, next_state=s2)
        sr13 = StateRelation.objects.create(condition=False, prev_state=s1, next_state=s3)
        sr23 = StateRelation.objects.create(prev_state=s2, next_state=s3)
        sr3end = StateRelation.objects.create(condition=False, prev_state=s3)
        self.fsa.start = s1
        self.fsa.save()
        result_True = s3.get_prev_state({'S1': {'state': True}, 'S2': {'state': None}})
        self.assertEqual(result_True, s2)
        result_False = s3.get_prev_state({'S1': {'state': False}})
        self.assertEqual(result_False, s1)


class TestFSA(CannedData, test.TestCase):

    def setUp(self):
        super(TestFSA, self).setUp()
        self.start = BooleanState.objects.create(
            name='START',
            **self.canned_data
        )
        self.fsa.start = self.start
        self.fsa.save()

    def generate_states(self):
        self.states = generate_states(self.start, **self.canned_data)

    def test_find_all_paths_one_state(self):
        srstart = StateRelation.objects.create(prev_state=self.start, next_state=None)
        self.assertEqual(self.fsa.find_all_paths(), [(self.start.name, None)])

    def test_find_all_paths(self):
        self.generate_states()
        paths = self.fsa.find_all_paths()
        route1 = (self.start.name, 'S1', 'S2', 'S4', 'S5', None)
        route2 = (self.start.name, 'S1', 'S3', 'S4', 'S5', None)
        self.assertEqual(set(paths), set((route1, route2)))
        self.assertNotEqual(route1, route2)

    def test_get_states_per_section(self):
        expected = [self.section.name, 'Things beloved by the emperor', 'Things that look like flies from a distance']
        for i, name in enumerate(expected[1:]):
            section = Section.objects.create(name=name, position=i, fsa=self.fsa)
            state = State.objects.create(name='S{}'.format(i), **self.canned_data)
            state.section = section
            state.save()
        result = self.fsa.get_states_per_section()
        self.assertEqual(list(result.keys()), expected)
        self.assertEqual(type(result[self.section.name]), type([]))

    def test_generate_text(self):
        self.maxDiff = None
        self.generate_states()
        t_START_text = 'Hulahoop'
        t_S1_text = 'Huuba'
        tstart = Text.objects.create(state=self.start, text=t_START_text)
        ts1 = Text.objects.create(state=self.states['s1'], text=t_S1_text)
        # Set bools to True
        data = {}
        for state in State.objects.filter(fsa=self.fsa):
            data[state.name] = {'state': True}
        result = self.fsa.generate_text(data)
        expected = [
            {
                'section': self.section.name,
                'introductory_text': self.section.introductory_text,
                'paragraphs': [
                    {'text': t_START_text, 'notes': ''},
                    {'text': t_S1_text, 'notes': ''},
                    {'notes': '', 'text': ''},
                    {'notes': '', 'text': ''},
                    {'notes': '', 'text': ''},
                    {'notes': '', 'text': ''},
                ],
            },
        ]
        self.assertEqual(result, expected)

    def test_get_maximal_previous_states(self):
        self.generate_states()
        result = self.fsa.get_maximal_previous_states('S4')
        self.assertEqual(len(result), 4)

    def test_scores_minimal(self):
        self.generate_states()
        score1 = Score.objects.create(fsa=self.fsa, name='score1', position=1)
        score2 = Score.objects.create(fsa=self.fsa, name='score2', position=2)
        self.states['s1'].scores.add(score1)
        self.states['s3'].scores.add(score2)
        data = {
            'START': {'state': False},
            'S1': {'state': False},
            'S2': {'state': False},
            'S3': {'state': False},
            'S4': {'state': False},
            'S5': {'state': False},
        }
        expected_score_set = {'score1', 'score2'}
        score_map, score_set = self.fsa.get_scores(data)
        self.assertEqual(expected_score_set, score_set)
        expected_score_map = {}
        self.assertEqual(expected_score_map, score_map)

    def test_scores_maximal(self):
        self.generate_states()
        score1 = Score.objects.create(fsa=self.fsa, name='score1', position=1)
        score2 = Score.objects.create(fsa=self.fsa, name='score2', position=2)
        self.states['s1'].scores.add(score1)
        self.states['s3'].scores.add(score2)
        data = {
            'START': {'state': True},
            'S1': {'state': True},
            'S2': {'state': True},
            'S4': {'state': True},
            'S5': {'state': True},
        }
        expected_score_set = {'score1'}
        score_map, score_set = self.fsa.get_scores(data)
        self.assertEqual(expected_score_set, score_set)
        expected_score_map = {'score1': 1}
        self.assertEqual(expected_score_map, score_map)

    def test_find_possible_paths_for_data(self):
        self.generate_states()
        expected_path = ('START', 'S1', 'S2', 'S4', 'S5')
        data = {k: None for k in expected_path[:3]}
        result = self.fsa.find_possible_paths_for_data(data)
        self.assertEqual(result, set((expected_path,)))
        bad_path = ('START', 'S1', 'S4', 'S5')
        data = {k: None for k in bad_path}
        result = self.fsa.find_possible_paths_for_data(data)
        self.assertEqual(len(result), 2)

    def test_order_data(self):
        self.generate_states()
        expected = [('START', None), ('S1', None), ('S2', None)]
        data = dict(expected)
        result = self.fsa.order_data(data)
        self.assertEqual(result, expected)
