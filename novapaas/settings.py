import json
import os

import dj_database_url

from dmpsite.settings.devel import *

# Fixed settings

ALLOWED_HOSTS = [
    '.paas2.uninett.no',
    '127.0.0.1',
    '0.0.0.0',
]

SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware'
]

try:
    DEBUG
except NameError:
    # Assure that DEBUG is set and safe
    DEBUG = False

STATIC_ROOT = 'staticfiles'

# Changeable settings

SECRET_KEY = os.getenv('SECRET_KEY', None)

DATABASE_URL = os.getenv('DMP_DATABASE_URL', None)
if DATABASE_URL:
    DATABASES = {
        'default': dj_database_url.parse(DATABASE_URL),
    }

DEBUG = bool(int(os.getenv('DEBUG', DEBUG)))

INSTALLED_APPS += [
    'social_django',
]

AUTHENTICATION_BACKENDS = [
    'dataporten.social.DataportenFeideOAuth2',
    'django.contrib.auth.backends.RemoteUserBackend',
    'django.contrib.auth.backends.ModelBackend',
]

SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ['username', 'email']

SOCIAL_AUTH_DATAPORTEN_FEIDE_KEY = 'dcddaabb-0cd9-4bff-b3b1-85a5a6654f87'
SOCIAL_AUTH_DATAPORTEN_FEIDE_SECRET = '15edfa22-82a3-4309-b43b-4aec7507f82f'

SOCIAL_AUTH_LOGIN_REDIRECT_URL = '/'
SOCIAL_AUTH_NEW_USER_REDIRECT_URL = SOCIAL_AUTH_LOGIN_REDIRECT_URL

SOCIAL_AUTH_DATAPORTEN_FEIDE_SSL_PROTOCOL = True

SOCIAL_AUTH_REDIRECT_IS_HTTPS = True
