from django.conf.urls import url

from .views import (
    NewPlanView,
    DeletePlanView,
    NewStateView,
    PlanListView,
    PlanDetailView,
    GeneratedPlanPlainTextView,
    GeneratedPlanHTMLView,
    GeneratedPlanPDFView,
)


PLAN_RE = r'(?P<plan>\d+)'
PLAN_PAGEROOT_RE = r'%s/' % PLAN_RE
STATE_RE = r'(?P<state>\w[.\w]{,15})'  # At least one letter, at most len(State.name))
STATE_PAGEROOT_RE = r'^%s/%s/' % (PLAN_RE, STATE_RE)

urlpatterns = [
    url(r'^$', PlanListView.as_view(), name='plan_list'),
    url(r'^new/$', NewPlanView.as_view(), name='new_plan'),
    url(STATE_PAGEROOT_RE + r'new/$', NewStateView.as_view(), name='new_state'),
    url(PLAN_PAGEROOT_RE + r'$', PlanDetailView.as_view(), name='plan_detail'),
    url(PLAN_PAGEROOT_RE + r'delete/$', DeletePlanView.as_view(), name='plan_delete'),
    url(PLAN_PAGEROOT_RE + r'generated.txt$', GeneratedPlanPlainTextView.as_view(), name='generated_plan_text'),
    url(PLAN_PAGEROOT_RE + r'generated.html$', GeneratedPlanHTMLView.as_view(), name='generated_plan_html'),
    url(PLAN_PAGEROOT_RE + r'generated.pdf$', GeneratedPlanPDFView.as_view(), name='generated_plan_pdf'),
]
