from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

SECRET_KEY = os.getenv('SECRET_KEY', None)

if not SECRET_KEY:
    del SECRET_KEY
