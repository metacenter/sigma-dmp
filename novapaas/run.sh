#!/bin/bash -xe

DJANGO_SETTINGS_MODULE="novapaas.settings"
export DJANGO_SETTINGS_MODULE

python3 manage.py collectstatic --noinput
#python3 manage.py migrate --noinput
gunicorn --log-level debug --access-logfile - novapaas.wsgi -b 0.0.0.0:$PORT
