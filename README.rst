Sigma-dmp
=========

How to play:

* make a virtualenv
* make sure to activate the virtualenv
* git clone git@bitbucket.org:metacenter/sigma-dmp.git
* cd sigma-dmp
* pip install -r requirements.txt
* python manage.py migrate
* python manage.py collectstatic --noinput

Then launch the dev-server locally:

* export SECRET_KEY="some_string_of_about_50_chars_or_so"
* export DJANGO_SETTINGS="novapass.settings"
* If using a non-local, non-sqlite3 database server::

   export DMP_DATABASE_URL="..", see docs for dj_database_url
* make a user: python manage.py createsuperuser
* python manage.py runserver

Run tests:

* pip install -r requirements/test.txt
* tox
