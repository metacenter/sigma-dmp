"""
WSGI config for nova.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application

from dj_static import Cling

NOVA_HOME = os.path.abspath('__file__')
APP_HOME = os.path.dirname(NOVA_HOME)
sys.path.append(os.path.join(APP_HOME, 'src'))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "novapaas.settings")

application = Cling(get_wsgi_application())
