from __future__ import unicode_literals

from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from jsonfield import JSONField

# With Django1.9 and postgres 9.4+, use this instead
# from django.contrib.postgres.fields import JSONField


@python_2_unicode_compatible
class DMPSchema(models.Model):
    plan_name = models.CharField(max_length=64)
    flowdata = JSONField(default={})
    previous_flowdata = JSONField(default={})
    added = models.DateTimeField(auto_now_add=True)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL)
    fsa = models.ForeignKey('flow.FSA', null=True)

    def __str__(self):
        return '%s (%s)' % (self.plan_name, self.added)
