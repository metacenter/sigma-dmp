# encoding: utf-8

from __future__ import unicode_literals

from collections import OrderedDict, Counter

from django.db import models
from django.utils.encoding import python_2_unicode_compatible, force_text


INPUT_TYPES = (
    'bool',
    'choice',
    'daterange',
    'multichoiceonetext',
)


def _force_items_to_str(dict_):
    return {force_text(k): force_text(v) for k, v in dict_.items()}


class OrderableMixin(models.Model):
    position = models.PositiveIntegerField(default=0, blank=False, null=False)

    class Meta:
        abstract = True


@python_2_unicode_compatible
class Section(OrderableMixin, models.Model):
    """Name of a section"""
    fsa = models.ForeignKey('FSA', related_name='sections')
    name = models.CharField(max_length=32, unique=True)
    introductory_text = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ('fsa', 'position')

    def __str__(self):  # pragma: no cover
        return self.name


@python_2_unicode_compatible
class Score(OrderableMixin, models.Model):
    """Name of a score"""
    fsa = models.ForeignKey('FSA', related_name='scores')
    name = models.CharField(max_length=16, unique=True)

    class Meta:
        ordering = ('fsa', 'position')

    def __str__(self):  # pragma: no cover
        return self.name


@python_2_unicode_compatible
class State(OrderableMixin, models.Model):
    """
    Defaults, fallbacks; interface for all states

    name: string state name for introspection, must be set
    next: next state, if no conditional involved
    section: what section the state is in
    question: question asked in the state
    framing_text: get_text() merges "texts" into this if set.
    help_text: more background about the question
    depends: State to look up to decide next state. If unset, lookup self

    Relations

    texts: used by get_text() to output one text string. Fallback: ''
    scores: which scores will be incremented if this state is True, if any
    next_states, prev_states: where to next
    """

    fsa = models.ForeignKey('FSA', related_name='states')
    name = models.CharField(max_length=16, unique=True)
    input_type = models.CharField(
        max_length=max(map(len, INPUT_TYPES)),
        choices=zip(INPUT_TYPES, INPUT_TYPES),
    )
    section = models.ForeignKey(Section)
    question = models.CharField(max_length=255)
    help_text = models.TextField(blank=True)
    framing_text = models.TextField(blank=True)
    scores = models.ManyToManyField(Score, blank=True)
    depends = models.ForeignKey(
        'self',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    class Meta:
        ordering = ('fsa', 'position')

    def _get_class(self):  # pragma: no cover
        return INPUT_TYPE_MAP[self.input_type]

    def _set_class(self, value):
        if value in INPUT_TYPE_MAP:
            self.__class__ = INPUT_TYPE_MAP[value]

    def __init__(self, *args, **kwargs):
        super(State, self).__init__(*args, **kwargs)
        self._set_class(self.input_type)

    def __setattr__(self, attr, value):
        if attr == 'input_type':
            self._set_class(value)
        return super(State, self).__setattr__(attr, value)

    def __str__(self):  # pragma: no cover
        return self.name

#     def clone(self, *args, **kwargs):
#         kwargs.pop('force_insert', None)
#         kwargs.pop('force_update', None)
#         suffix = '-clone'
#         self.name = self.name[:16-len(suffix)] + suffix
#         self.save(force_insert=True, *args, **kwargs)
#         return self

    def _serialize_condition(self, _):
        """
        Convert an answer into a lookup key, if applicable
        """
        return NotImplemented

    def get_text(self, answer, **kwargs):
        if self.texts.count() == 0:
            kwargs['text'] = ''
        elif self.texts.count() == 1:
            kwargs['text'] = self.texts.get().text
        else:
            condition = self._serialize_condition(answer)
            text = self.texts.get(condition=condition)
            kwargs['text'] = text.text
        # In Python 2.7, kwargs.keys are bytes, not str
        return _force_items_to_str(kwargs)

    def get_next_state_many(self, next_states, states):
        return NotImplemented

    def get_next_state(self, states):
        next_states = self.next_states.all()
        if next_states.count() == 1:  # simple state
            return next_states.get().next_state
        elif next_states.count() > 1:  # complex state
            return self.get_next_state_many(next_states, states)
        # end state or overridden
        return None

    def get_prev_state_many(self, prev_states, states):
        states = states.copy()
        states[self.name] = None
        paths = self.fsa.find_paths_to(self.name)
        ordered_states = tuple([s[0] for s in self.fsa.order_data(states)])
        good_path = None
        for path in paths:
            if path[:len(ordered_states)] == ordered_states:
                good_path = path
                break
        prev_statename = good_path[good_path.index(self.name)-1]
        prev_state = self.fsa.statemap[prev_statename]
        return prev_state

    def get_prev_state(self, states):
        prev_states = self.prev_states.all()
        if prev_states.count() == 1:  # simple state
            return prev_states.get().prev_state
        elif prev_states.count() > 1:  # complex state
            return self.get_prev_state_many(prev_states, states)
        # start state or overridden
        return None


@python_2_unicode_compatible
class StateRelation(models.Model):
    condition = models.NullBooleanField()
    prev_state = models.ForeignKey(
        State,
        related_name='next_states',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )
    next_state = models.ForeignKey(
        State,
        related_name='prev_states',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    class Meta:
        unique_together = ('prev_state', 'next_state')

    def __str__(self):
        me = self.prev_state
        return '{} ({} is {}) -> {}'.format(me, me, self.condition, self.next_state)


@python_2_unicode_compatible
class Text(models.Model):
    state = models.ForeignKey(
        State,
        related_name='texts',
        on_delete=models.CASCADE,
    )
    condition = models.CharField(max_length=32, blank=True)
    text = models.TextField(blank=True, null=True)

    def __str__(self):
        return '{}: "{}" {}'.format(self.state.name, self.condition, self.text)


class BooleanState(State):

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        self.input_type = 'bool'
        super(BooleanState, self).save(*args, **kwargs)

    def _serialize_condition(self, answer):
        """
        answer in (True, False)
        """
        if answer is True:
            return 'TRUE'
        return 'FALSE'

    def get_next_state_many(self, next_states, states):
        depends = self.depends if self.depends else self
        key = states[depends.name]['state']
        next_state = next_states.get(condition=key)
        return next_state.next_state


class ChoiceState(State):

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        self.input_type = 'choice'
        super(ChoiceState, self).save(*args, **kwargs)

    def _serialize_condition(self, answer):
        """
        Return answer unchanged
        """
        return answer


class MultipleChoiceOneTextState(State):

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        self.input_type = 'multichoiceonetext'
        super(MultipleChoiceOneTextState, self).save(*args, **kwargs)

    def get_text(self, answer, **kwargs):
        """
        answer = ['list', 'of', 'answers']
        """
        if len(answer) == 1:
            joined_answer = answer[0]
        else:
            joined_answer = '{} and {}'.format(', '.join(answer[:-1]), answer[-1])
        kwargs['text'] = joined_answer
        if self.framing_text:
            kwargs['text'] = self.framing_text.format(joined_answer)
        return _force_items_to_str(kwargs)


class DateRangeState(State):

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        self.input_type = 'daterange'
        super(DateRangeState, self).save(*args, **kwargs)

    def get_text(self, daterange, **kwargs):
        """
        daterange = {
            'start': date(),
            'end': date(),
        }
        """
        kwargs['text'] = self.framing_text.format(**daterange)
        return _force_items_to_str(kwargs)


INPUT_TYPE_MAP = {
    'bool': BooleanState,
    'choice': ChoiceState,
    'daterange': DateRangeState,
    'multichoiceonetext': MultipleChoiceOneTextState,
}


class FSA(models.Model):
    name = models.CharField(max_length=16)
    start = models.ForeignKey(
        'State',
        related_name='+',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    class Meta:
        verbose_name = 'FSA'
        verbose_name_plural = 'FSAs'

    def __str__(self):  # pragma: no cover
        return self.name

    @property
    def statemap(self):
        return {s.name: s for s in self.states.all()}

    def generate_graph(self):
        graph = {}
        for state in self.states.all():
            goes_to = list()
            for s in state.next_states.all():
                goes_to.append(s.next_state.name if s.next_state else None)
            graph[state.name] = goes_to
        return graph

    def _find_all_paths(self, graph, start, path=[]):
        path = path + [start]
        if start is None:
            return [path]
        if start not in graph:
            return []
        paths = []
        for node in graph[start]:
            if node not in path:
                newpaths = self._find_all_paths(graph, node, path)
                for newpath in newpaths:
                    paths.append(newpath)
        return paths

    def find_all_paths(self):
        graph = self.generate_graph()
        paths = self._find_all_paths(graph, self.start.name)
        return [tuple(path) for path in paths]

    def find_paths_to(self, statename):
        paths = self.find_all_paths()
        paths_to = []
        for path in paths:
            if statename in path:
                pos = path.index(statename)
                paths_to.append(path[:pos])
        paths_to.append(statename)
        return paths

    def find_paths_from(self, statename):
        paths = self.find_all_paths()
        paths_from = []
        for path in paths:
            if statename in path:
                pos = path.index(statename)
                paths_from.append(path[pos:])
        return paths_from

    def nextstate(self, curstate, data):
        "Get next state"
        return self.statemap[curstate].get_next_state(data)

    def prevstate(self, curstate, data):
        "Get prev state"
        return self.statemap[curstate].get_prev_state(data)

    def get_maximal_previous_states(self, statename):
        state = self.statemap[statename]
        states = self.states.filter(position__lt=state.position).order_by('position')
        return list(states)

    def get_states_per_section(self):
        all_sections = Section.objects.filter(fsa=self).order_by('position')
        all_states = self.states.order_by('position')
        sections = dict()
        for state in all_states:
            statename = state.name
            sectionname = state.section.name
            sections[sectionname] = sections.get(sectionname, [])
            sections[sectionname].append(statename)
        ordered_sections = OrderedDict()
        for section in all_sections.values_list('name', flat=True):
            if section in sections:
                ordered_sections[section] = sections[section]
        return ordered_sections

    def generate_text(self, data):
        paragraphs = []
        sections = self.get_states_per_section()
        for sectionname, statenames in sections.items():
            section = Section.objects.get(name=sectionname, fsa=self)
            paragraphs.append({'section': sectionname})
            section_text = []
            for statename in statenames:
                state = self.statemap[statename]
                if statename in data:
                    condition = data[statename]['state']
                    notes = data[statename].get('notes', '')
                    text = state.get_text(condition, notes=notes)
                    if filter(None, text):
                        section_text.append(text)
            paragraphs[-1]['paragraphs'] = section_text
            paragraphs[-1]['introductory_text'] = section.introductory_text
        return paragraphs

    def get_scores(self, data):
        score_map = Counter()
        score_set = set()
        data_map = dict(data)
        for statename, state in self.statemap.items():
            scores = [score.name for score in state.scores.all()]
            if not scores or (statename not in data_map):
                continue
            score_set.update(scores)
            if data_map[statename]['state'] in ('Yes', True):
                score_map.update(scores)
        return OrderedDict(sorted(score_map.items())), score_set

    def find_possible_paths_for_data(self, data):
        paths = self.find_all_paths()
        candidate_paths = set()
        keys = set(data.keys())
        for path in paths:
            path = tuple(state for state in path if state)
            if keys.issubset(path):
                candidate_paths.add(path)
        return candidate_paths

    def order_data(self, data):
        """Sort data according to possible graphs

        Incidentally, removes unknown states"""
        # All graphs start the same so just pick one
        keys = self.find_possible_paths_for_data(data).pop()
        ordered_data = [(s, data.get(s)) for s in keys if s in data]
        return ordered_data
