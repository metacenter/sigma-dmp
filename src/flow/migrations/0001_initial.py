# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-04-27 10:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='FSA',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=16)),
            ],
        ),
        migrations.CreateModel(
            name='Score',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.PositiveIntegerField(default=0)),
                ('name', models.CharField(max_length=16, unique=True)),
                ('fsa', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='scores', to='flow.FSA')),
            ],
            options={
                'ordering': ('fsa', 'position'),
            },
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.PositiveIntegerField(default=0)),
                ('name', models.CharField(max_length=32, unique=True)),
                ('fsa', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sections', to='flow.FSA')),
            ],
            options={
                'ordering': ('fsa', 'position'),
            },
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.PositiveIntegerField(default=0)),
                ('name', models.CharField(max_length=16, unique=True)),
                ('input_type', models.CharField(choices=[('bool', 'bool'), ('choice', 'choice'), ('daterange', 'daterange'), ('multichoiceonetext', 'multichoiceonetext')], max_length=18)),
                ('question', models.CharField(max_length=255)),
                ('help_text', models.TextField(blank=True)),
                ('framing_text', models.TextField(blank=True)),
                ('depends', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='flow.State')),
                ('fsa', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='states', to='flow.FSA')),
                ('scores', models.ManyToManyField(blank=True, to='flow.Score')),
                ('section', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='flow.Section')),
            ],
            options={
                'ordering': ('fsa', 'position'),
            },
        ),
        migrations.CreateModel(
            name='StateRelation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('condition', models.NullBooleanField()),
                ('next_state', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='prev_states', to='flow.State')),
                ('prev_state', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='next_states', to='flow.State')),
            ],
        ),
        migrations.CreateModel(
            name='Text',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('condition', models.CharField(blank=True, max_length=32)),
                ('text', models.TextField(blank=True, null=True)),
                ('state', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='texts', to='flow.State')),
            ],
        ),
        migrations.AddField(
            model_name='fsa',
            name='start',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='flow.State'),
        ),
        migrations.CreateModel(
            name='BooleanState',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('flow.state',),
        ),
        migrations.CreateModel(
            name='ChoiceState',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('flow.state',),
        ),
        migrations.CreateModel(
            name='DateRangeState',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('flow.state',),
        ),
        migrations.CreateModel(
            name='MultipleChoiceOneTextState',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('flow.state',),
        ),
        migrations.AlterUniqueTogether(
            name='staterelation',
            unique_together=set([('prev_state', 'next_state')]),
        ),
    ]
