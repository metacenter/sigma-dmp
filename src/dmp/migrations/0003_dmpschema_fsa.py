# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-24 11:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('flow', '0004_section_introductory_text'),
        ('dmp', '0002_dmpschema_previous_flowdata'),
    ]

    operations = [
        migrations.AddField(
            model_name='dmpschema',
            name='fsa',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='flow.FSA'),
        ),
    ]
