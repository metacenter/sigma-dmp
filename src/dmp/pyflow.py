# encoding: utf-8

from __future__ import unicode_literals

from collections import OrderedDict, Counter


class AbstractState(object):
    """Defaults, fallbacks; interface for all states

    name: string state name for introspection, must be set
    next: next state, if no conditional involved
    section: what section the state is in
    question: question asked in the state
    text: used by get_text() to output one text string. Fallback: ''
    optional: whether a state is skippable, True. Fallback: False
    scores: which scores will be incremented if this state is True, if any
    """
    name = None
    next = None
    section = 'Miscellaneous'
    question = ''
    text = ''
    optional = False
    scores = set()

    def __init__(self, data):
        self.state = data

    def nextstate(self):
        "Depending on state, return the name of the next state"
        if self.next:
            if getattr(self, 'depends', None):
                # complex state
                return self.next[self.state[self.depends]['state']]
            # simple state
            return self.next
        # final state
        return None

    def get_text(self):
        text = ' '.join(self.text.split())
        notes = self.state[self.name].get('notes', '')
        return [text, notes]


class BoolTextMixin(object):
    """Returns text depending on a boolean answer

    text must be a dict with keys True, False and the text to return as values
    """

    def get_text(self):
        text = ' '.join(self.text[self.state[self.name]['state']].split())
        notes = self.state[self.name].get('notes', '')
        return [text, notes]


class ChoiceTextMixin(object):
    """Returns text depending on a non-boolean choice

    text must be a dict with the choices as keys and the text to return as values
    """

    def get_text(self):
        text =  ' '.join(self.text[self.state[self.name]['state']].split())
        notes = self.state[self.name].get('notes', '')
        return [text, notes]


class MultipleChoiceOneTextMixin(object):
    """Returns multiple paragraphs of text depending on non-boolean choices

    choices: must be a dict with the choices as keys and what to show as values
    text: a string with a single format-field for concatenated choices, like so:

        text = "Choices made: {}"
    """

    def get_text(self):
        pieces = self.state[self.name]['state']
        choice_string = ', '.join(pieces)
        text = self.text.format(choice_string)
        notes = self.state[self.name].get('notes', '')
        return [text, notes]

class DateRangeTextMixin(object):
    """Returns text defining and describing a daterange

    text is a string containing {start} and {end}, like so:
    "This will be going on between {start} and {end}, inclusive"
    """

    def get_text(self):
        daterange = {
            'start': self.state[self.name]['state']['start'],
            'end': self.state[self.name]['state']['end'],
        }
        text = ' '.join(self.text.format(**daterange).split())
        notes = self.state[self.name].get('notes', '')
        return [text, notes]


class BoolState(BoolTextMixin, AbstractState):
    input_type = 'bool'
    text = {
        True: '',
        False: '',
    }


class ChoiceState(ChoiceTextMixin, AbstractState):
    input_type = 'choice'


class MultipleChoiceOneTextState(MultipleChoiceOneTextMixin, AbstractState):
    input_type = 'multichoiceonetext'


class DateRangeState(DateRangeTextMixin, AbstractState):
    input_type = 'daterange'
    text = "{start} _> {end}"


class A1State(BoolState):
    name = 'A1'
    next = 'A2'
    section = 'General'
    question = """Will accessibility to any of the data you collect threaten
            individuals or national security?"""


class A2State(BoolState):
    name = 'A2'
    next = 'Q7'
    section = 'General'
    optional = True
    question = """Will accessibility to any of the data you collect be in
            violation of “Personopplysningsloven” or “Helseregisterloven”?"""


class Q7State(BoolState):
    name = 'Q7'
    next = 'A3'
    section = 'General'
    question = """Will any of the data collected contain sensitive data
            (information in violation of “Personopplysningsloven” or
            “Helseregisterloven”)"""
    text = {
        True: """The data contains sensitive information and approval from the
                Research Ethics Committee will be sought and any
                recommendations from the committee will be followed
                through.""",
        False: """The data will not contain sensitive information that is in
                violation of “Personopplysningsloven” or “Helseregisterloven”.""",
    }


class A3State(BoolState):
    name = 'A3'
    next = 'A4'
    section = 'General'
    question = """Will accessibility to any of the data you collect be in
            conflict with judicial regulations?"""


class A4State(BoolState):
    name = 'A4'
    next = 'A5'
    section = 'General'
    question = "Will the data have commercial value?"


class A5State(BoolState):
    name = 'A5'
    next = 'Q8'
    section = 'General'
    question = """Will accessibility to the data have great economic or
            practical consequences for whom generated/collected the data?"""


class Q1State(BoolState):
    name = 'Q1'
    next = 'Q2'
    section = 'Data Management'
    question = "Will the project be making use of existing data?"
    text = {
        True: """The project will be making use of existing data. The project
                manager will be responsible for the securing the appropriate
                permissions for the data in order to allow open access
                publication of this data. It is recommended that data under
                licenses such as NLOD or CC By 4.0 be used.""",
        False: """The project will be based on data produced solely during the
        project.""",
    }


class Q2State(MultipleChoiceOneTextState):
    name = 'Q2'
    next = 'Q3'
    section = 'Data Management'
    question = "What are the primary sources of creating or collecting data?"
    text = "The primary sources will be: {}."
    choices = OrderedDict(
        (
            ('notur', 'notur'),
            ('institutional computing', 'institutional computing'),
            ('commercial cloud service', 'commercial cloud service'),
            ('gene sequencer', 'gene sequencer'),
            ('satellite', 'satellite'),
            ('marine sensors', 'marine sensors'),
            ('earth sensors', 'earth sensors'),
            ('atmospheric sensors', 'atmospheric sensors'),
            ('microscope', 'microscope'),
        )
    )


class Q3State(DateRangeState):
    name = 'Q3'
    next = 'Q9a'
    section = 'Data Management'
    question = "What are the expected periods of creating or collecting data:"
    text = "During the course of the project data will be collected from {start} to {end}."
    help_text = "First box is from, inclusive, second box is to, inclusive. Format is YYYY-MM-DD"


class Q4aState(BoolState):
    name = 'Q4a'
    section = 'Metadata'
    question = "Will metadata be collected along with the data?"
    text = {
        True: """Throughout the project, metadata will be collected and
                associated with the data. This metadata will describe the data,
                how the data was collected and how to use the data. The
                metadata will be stored such that it is possible to
                unambiguously access the metadata for any given data (linking
                metadata and data).""",
        False: "No metadata will be collected for the data.",
    }
    depends = name
    next = {
        True: 'Q4b',
        False: 'Q5a',
    }


class Q4bState(BoolState):
    name = 'Q4b'
    next = 'Q5a'
    section = 'Metadata'
    optional = True
    scores = {'S4'}
    question = "Will the metadata be compliant with a metadata standard?"
    text = {
        True: """The metadata will follow the metadata standards adopted by the
                community. The metadata documents will be verified against the
                schema.""",
        False: "The metadata will not be compliant with a standard.",
    }


class Q4cState(BoolState):
    name = 'Q4c'
    next = 'Q9c'
    optional = True
    scores = {'S4'}
    section = 'Publication'
    question = "Will the published data be accompanied with searchable metadata?"
    text = {
        True: """Searchable metadata will be provided for the published data.
                The metadata will conform to the schema specified by the
                publishing agent.""",
        False: "Searchable metadata will not be provided for the published data.",
    }


class Q5aState(ChoiceState):
    name = 'Q5a'
    next = 'Q5b'
    section = 'Data Management'
    question = "Select the data classification appropriate for the data:"
    text = OrderedDict(
        (
        ('critical', """The data produced is considered to be critical to the
                project. It will not be possible to successfully complete the
                project without this data."""),
        ('reproducible', """The data produced is considered to be reproducible.
                If the data is lost or damaged it is technically possible to
                recreate the data. Reproducing the data may come at a
                significant expense, both in terms of time and cost."""),
        ('duplicate', """Copies of the data exist and loss of the data can
                therefore be recovered from a duplicate. This may come at an
                additional cost."""),
        )
    )


class Q5bState(BoolState):
    name = 'Q5b'
    next = 'Q6a'
    section = 'Data Management'
    question = "Is the data structured? (i.e. searchable, suitable for relational database)"
    text = {
        True: "The data is highly structured and possible to search.",
        False: "The data is unstructured and non searchable.",
    }


class Q6aState(BoolState):
    name = 'Q6a'
    scores = {'S2'}
    section = 'Publication'
    question = "Will the data be published for open access with a persistent identifier?"
    text = {
        True: 'The data be published for open access with a persistent identifier.',
        False: "The data will not be published.",
    }
    depends = name
    next = {
        True: 'Q6b',
        False: None,
    }


class Q6bState(BoolState):
    name = 'Q6b'
    next = 'Q4c'
    optional = True
    scores = {'S5'}
    section = 'Publication'
    question = "Will the published data be provided with international licences for open access?"
    text = {
        True: """The project will ensure the published data are free from
                restrictions and abide by the CC By 4.0 or other international
                licenses for open access to data.""",
        False: "The published data will not abide by CC By 4.0 or any other licenses for open access.",
    }


class Q8State(ChoiceState):
    name = 'Q8'
    next = 'Q1'
    section = 'Data Management'
    question = "Approximately how much data will be collected during the project?"
    text = OrderedDict((
            ('< 1TB', "The project is expected to produce a total amount of small quantities of data."),
            ('1‒100TB', "The project is expected to produce a total amount of medium quantities of data."),
            ('>100TB', "The project is expected to produce a total amount of large quantities of data."),
        )
    )


class Q9aState(BoolState):
    name = 'Q9a'
    next = 'Q9b'
    section = 'Data Management'
    question = "Will the project be using the UNINETT Sigma2 infrastructure for computing?"
    text = {
        True: "The project requires access to the UNINETT Sigma2 computing resources.",
        False: "The project does not require access to the UNINETT Sigma2 computing resources.",
    }


class Q9bState(BoolState):
    name = 'Q9b'
    next = 'Q4a'
    section = 'Data Management'
    question = "Will the project be using the UNINETT Sigma2 infrastructure for active data?"
    text = {
        True: "The project requires access to the UNINETT Sigma2 project area for active data.",
        False: "The project does not require access to the UNINETT Sigma2 project area for active data..",
    }


class Q9cState(BoolState):
    name = 'Q9c'
    next = 'Q10a'
    optional = True
    scores = {'S2', 'S1'}
    section = 'Publication'
    question = "Will the project be using the UNINETT Sigma2 service for data archiving?"
    text = {
        True: "The project will publish data via the UNINETT Sigma2 archive service.",
        False: "The project will not publish data via the UNINETT Sigma2 archive service.",
    }


class Q10aState(BoolState):
    name = 'Q10a'
    next = 'Q10b'
    optional = True
    section = 'Publication'
    question = """Will the project publish the relevant data for refereed
            scientific publications within the date of publication?"""

    text = {
        True: """The project plans to publish the data relevant to the
                scientific articles such that the data are openly accessible
                within the date of the publication.""",
        False: """The project does not plan to publish the data relevant to the
                scientific articles at the time of publication.""",
    }


class Q10bState(BoolState):
    name = 'Q10b'
    next = None  # Final state
    optional = True
    scores = {'S3'}
    section = 'Publication'
    question = """Will the project publish other relevant data within three (3)
            years of completing the project?"""
    text = {
        True: """The project plans to make other relevant data openly available
                within three years of the project end-date.""",
        False: """The project does not plan to make other relevant data openly
                available within three years of the project end-date."""
    }


class FSA(object):
    states = (
        ('A1', A1State),
        ('A2', A2State),
        ('Q7', Q7State),
        ('A3', A3State),
        ('A4', A4State),
        ('A5', A5State),
        ('Q8', Q8State),
        ('Q1', Q1State),
        ('Q2', Q2State),
        ('Q3', Q3State),
        ('Q9a', Q9aState),
        ('Q9b', Q9bState),
        ('Q4a', Q4aState),
        ('Q4b', Q4bState),
        ('Q5a', Q5aState),
        ('Q5b', Q5bState),
        ('Q6a', Q6aState),
        ('Q6b', Q6bState),
        ('Q4c', Q4cState),
        ('Q9c', Q9cState),
        ('Q10a', Q10aState),
        ('Q10b', Q10bState),
    )
    statemap = OrderedDict(states)
    ordered_statenames = statemap.keys()
    start = 'A1'
    end = {'Q6a', 'Q10b'}

    @classmethod
    def order_data(cls, data):
        """Sort data according to the hardcoded order of states

        Incidentally, removes unknown states"""
        keys = cls.ordered_statenames
        ordered_data = [(s, data.get(s)) for s in keys if s in data]
        return ordered_data

    @classmethod
    def get_maximal_previous_states(cls, statename):
        output = []
        for state in cls.statemap.values():
            if state.name == statename:
                break
            output.append(state)
        return output

    @classmethod
    def get_latest_state(cls, data=None):
        "Get latest state"
        if not data:
            return cls.start
        return cls.order_data(data)[-1][0]

    @classmethod
    def nextstate(cls, curstate, data):
        "Get next state"
        return cls.statemap[curstate](data).nextstate()

    @classmethod
    def get_states_per_section(cls):
        sections = OrderedDict()
        for statename, stateclass in cls.statemap.items():
            sectionname = stateclass.section
            sections[sectionname] = sections.get(sectionname, [])
            sections[sectionname].append(statename)
        return sections

    @classmethod
    def generate_text(cls, data):
        paragraphs = []
        sections = cls.get_states_per_section()
        for section, statenames in sections.items():
            paragraphs.append({'section': section})
            section_text = []
            for statename in statenames:
                stateclass = cls.statemap[statename]
                if statename in data:
                    state = stateclass(data)
                    text = state.get_text()
                    if text != ['', '']:
                        section_text.append(text)
            paragraphs[-1]['paragraphs'] = section_text
        return paragraphs

    @classmethod
    def get_scores(cls, data):
        score_map = Counter()
        score_set = set()
        data_map = dict(data)
        for statename, stateclass in cls.statemap.items():
            scores = stateclass.scores
            if not scores or not statename in data_map:
                continue
            score_set.update(scores)
            state = stateclass(data)
            if data_map[statename]['state'] in ('Yes', True):
                score_map.update(scores)
        return OrderedDict(sorted(score_map.items())), score_set
