from django.contrib import admin

from .models import DMPSchema


class DMPSchemaAdmin(admin.ModelAdmin):
    list_display = ['plan_name', 'added_by', 'added']
    readonly_fields = ['added']
admin.site.register(DMPSchema, DMPSchemaAdmin)
