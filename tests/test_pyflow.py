# encoding: utf-8

from __future__ import unicode_literals

import unittest
from datetime import date
from collections import OrderedDict

from dmp.pyflow import Q1State, Q2State, Q3State, Q5aState
from dmp.pyflow import Q4aState, Q6aState, Q10bState
from dmp.pyflow import FSA


class TestStateGetTextMethods(unittest.TestCase):

    def test_default_bool_get_text(self):
        data = {'Q1': {'state': True, 'notes': ''}}
        expected1 = "The project will be making use of existing data."
        result1 = Q1State(data).get_text()[0]
        self.assertEqual(result1[:len(expected1)], expected1)
        data = {'Q1': {'state': False, 'notes': ''}}
        expected2 = "The project will be based on data produced"
        result2 = Q1State(data).get_text()[0]
        self.assertEqual(result2[:len(expected2)], expected2)
        self.assertNotEqual(result1, result2)

    def test_bool_get_text(self):
        data = {'Q1': {'state': True, 'notes': ''}}
        expected = "The project will be making use of existing data."
        result = Q1State(data).get_text()[0]
        self.assertEqual(result[:len(expected)], expected)
        data = {'Q1': {'state': False, 'notes': ''}}
        expected = "The project will be based on data produced solely"
        result = Q1State(data).get_text()[0]
        self.assertEqual(result[:len(expected)], expected)

    def test_list_get_text(self):
        data = {'Q5a': {'state': 'critical', 'notes':''}}
        expected = "The data produced is considered to be critical"
        result = Q5aState(data).get_text()[0]
        self.assertEqual(result[:len(expected)], expected)

    def test_multilist_get_text(self):
        data = {'Q2': {'state': ['notur', 'institutional computing'], 'notes':''}}
        expected = "The primary sources will be: notur, institutional computing."
        result = Q2State(data).get_text()[0]
        self.assertEqual(result, expected)

    def test_daterange_get_text(self):
        daterange = {'start': date(1980, 1, 1), 'end': date(1990, 1, 1)}
        data = {'Q3': {'state': daterange, 'notes': ''}}
        expected = Q3State.text.format(**daterange)
        result = Q3State(data).get_text()[0]
        self.assertEqual(result, expected)


class TestStateNextstateMethods(unittest.TestCase):

    def test_default_nextstate(self):
        data = {'Q1': None}
        state = Q1State(data)
        result = state.nextstate()
        self.assertEqual(result, state.next)

    def test_Q4a_nextstate(self):
        data = {'Q4a': {'state': True, 'notes': ''}}
        state = Q4aState(data)
        result = state.nextstate()
        self.assertEqual(result, 'Q4b')
        data = {'Q4a': {'state': False, 'notes': ''}}
        state = Q4aState(data)
        result = state.nextstate()
        self.assertEqual(result, 'Q5a')

    def test_Q6a_nextstate(self):
        data = {'Q6a': {'state': True, 'notes': ''}}
        state = Q6aState(data)
        result = state.nextstate()
        self.assertEqual(result, 'Q6b')
        data = {'Q6a': {'state': False, 'notes': ''}}
        state = Q6aState(data)
        result = state.nextstate()
        self.assertEqual(result, None)

    def test_Q10b_nextstate(self):
        data = {'Q10b': {'state': True, 'notes': ''}}
        state = Q10bState(data)
        result1 = state.nextstate()
        self.assertEqual(result1, None)
        data = {'Q10b': {'state': False, 'notes': ''}}
        state = Q10bState(data)
        result2 = state.nextstate()
        self.assertEqual(result1, result2)

class TestFSA(unittest.TestCase):

    def setUp(self):
        self.data_all = (
            ('A1', {'state': None, 'notes': ''}),
            ('A2', {'state': None, 'notes': ''}),
            ('Q7', {'state': None, 'notes': ''}),
            ('A3', {'state': None, 'notes': ''}),
            ('A4', {'state': None, 'notes': ''}),
            ('A5', {'state': None, 'notes': ''}),
            ('Q8', {'state': None, 'notes': ''}),
            ('Q1', {'state': None, 'notes': ''}),
            ('Q2', {'state': None, 'notes': ''}),
            ('Q3', {'state': None, 'notes': ''}),
            ('Q9a', {'state': None, 'notes': ''}),
            ('Q9b', {'state': None, 'notes': ''}),
            ('Q4a', {'state': None, 'notes': ''}),
            ('Q4b', {'state': None, 'notes': ''}),
            ('Q5a', {'state': None, 'notes': ''}),
            ('Q5b', {'state': None, 'notes': ''}),
            ('Q6a', {'state': None, 'notes': ''}),
            ('Q6b', {'state': None, 'notes': ''}),
            ('Q4c', {'state': None, 'notes': ''}),
            ('Q9c', {'state': None, 'notes': ''}),
            ('Q10a', {'state': None, 'notes': ''}),
            ('Q10b', {'state': None, 'notes': ''}),
        )
        self.data_minimal = (
            ('A1', {'state': None, 'notes': ''}),
            ('A2', {'state': None, 'notes': ''}),
            ('Q7', {'state': None, 'notes': ''}),
            ('A3', {'state': None, 'notes': ''}),
            ('A4', {'state': None, 'notes': ''}),
            ('A5', {'state': None, 'notes': ''}),
            ('Q8', {'state': None, 'notes': ''}),
            ('Q1', {'state': None, 'notes': ''}),
            ('Q2', {'state': None, 'notes': ''}),
            ('Q3', {'state': None, 'notes': ''}),
            ('Q9a', {'state': None, 'notes': ''}),
            ('Q9b', {'state': None, 'notes': ''}),
            ('Q4a', {'state': False, 'notes': ''}),
#             ('Q4b', {'state': None, 'notes': ''}),
            ('Q5a', {'state': None, 'notes': ''}),
            ('Q5b', {'state': None, 'notes': ''}),
            ('Q6a', {'state': False, 'notes': ''}),
#             ('Q6b', {'state': None, 'notes': ''}),
#             ('Q4c', {'state': None, 'notes': ''}),
#             ('Q9c', {'state': None, 'notes': ''}),
#             ('Q10a', {'state': None, 'notes': ''}),
#             ('Q10b', {'state': None, 'notes': ''}),
        )
        self.data_maximal = (
            ('A1', {'state': None, 'notes': ''}),
            ('A2', {'state': None, 'notes': ''}),
            ('Q7', {'state': None, 'notes': ''}),
            ('A3', {'state': None, 'notes': ''}),
            ('A4', {'state': None, 'notes': ''}),
            ('A5', {'state': None, 'notes': ''}),
            ('Q8', {'state': None, 'notes': ''}),
            ('Q1', {'state': None, 'notes': ''}),
            ('Q2', {'state': None, 'notes': ''}),
            ('Q3', {'state': None, 'notes': ''}),
            ('Q9a', {'state': None, 'notes': ''}),
            ('Q9b', {'state': None, 'notes': ''}),
            ('Q4a', {'state': True, 'notes': ''}),
            ('Q4b', {'state': None, 'notes': ''}),
            ('Q5a', {'state': None, 'notes': ''}),
            ('Q5b', {'state': None, 'notes': ''}),
            ('Q6a', {'state': True, 'notes': ''}),
            ('Q6b', {'state': None, 'notes': ''}),
            ('Q4c', {'state': None, 'notes': ''}),
            ('Q9c', {'state': True, 'notes': ''}),
            ('Q10a', {'state': None, 'notes': ''}),
            ('Q10b', {'state': None, 'notes': ''}),
        )
        self.second_state = 'A2'

    def test_start(self):
        fsa = FSA()
        data = {}
        curstate = fsa.get_latest_state(data)
        self.assertEqual(curstate, FSA.start)

    def test_order_data_one_item(self):
        item = self.data_all[:1]
        data = dict(item)
        ordered_data = FSA.order_data(data)
        self.assertEqual(item, tuple(ordered_data))

    def test_order_data(self):
        data = dict(self.data_all)
        ordered_data = FSA.order_data(data)
        self.assertEqual(self.data_all, tuple(ordered_data))

    def test_get_latest_state_unaffected_by_value(self):
        fsa = FSA()
        data = {'Q1': "won't be checked"}
        curstate = fsa.get_latest_state(data)
        self.assertEqual(curstate, 'Q1')
        data = {'Q1': "will be ignored"}
        curstate = fsa.get_latest_state(data)
        self.assertEqual(curstate, 'Q1')

    def test_get_latest_state(self):
        fsa = FSA()
        data = dict(self.data_maximal[:1])
        curstate = fsa.get_latest_state(data)
        self.assertEqual(curstate, FSA.start)
        data = dict(self.data_all[:5])
        curstate = fsa.get_latest_state(data)
        self.assertEqual(curstate, 'A4')
        data = dict(self.data_all)
        curstate = fsa.get_latest_state(data)
        self.assertIn(curstate, FSA.end)

    def test_independent_stateswitch(self):
        fsa = FSA()
        data = {FSA.start: "won't be checked"}
        nextstate1 = fsa.nextstate(FSA.start, data)
        self.assertEqual(nextstate1, self.second_state)
        data = {FSA.start: "will be ignored"}
        nextstate2 = fsa.nextstate(FSA.start, data)
        self.assertEqual(nextstate1, nextstate2)

    def test_Q4a_stateswitch(self):
        fsa = FSA()
        curstate = 'Q4a'
        data = {curstate: {'state': True, 'notes': ''}}
        nextstate = fsa.nextstate(curstate, data)
        self.assertEqual(nextstate, 'Q4b')
        data = {curstate: {'state': False, 'notes': ''}}
        nextstate = fsa.nextstate(curstate, data)
        self.assertEqual(nextstate, 'Q5a')

    def test_end(self):
        fsa = FSA()
        curstate = 'Q10b'
        data = {curstate: {'state': 'whatever'}}
        nextstate1 = fsa.nextstate(curstate, data)
        self.assertEqual(nextstate1, None)
        data = OrderedDict(
            (
                ('Q9c', True),
                ('Q10a', True),
                ('Q10b', True),
            )
        )
        nextstate2 = fsa.nextstate(curstate, data)
        self.assertEqual(nextstate1, nextstate2)

    def run_flow(self):
        data = self.test_data
        fsa = FSA()
        curstate = FSA.start
        nextstate = self.second_state
        i = 1
        maxstates = len(data) + 1  # needed to switch into final state
        while nextstate is not None:
            data_subset = dict(data[:i])
            i += 1
            self.assertLessEqual(i, maxstates)
            lateststate = fsa.get_latest_state(data_subset)
            nextstate = fsa.nextstate(curstate, data_subset)
            curstate = nextstate
        self.assertEqual(curstate, None)

    def test_run_minimal(self):
        self.test_data = self.data_minimal
        self.run_flow()

    def test_run_maximal(self):
        self.test_data = self.data_maximal
        self.run_flow()

    def test_scores_minimal(self):
        data = self.data_minimal
        expected_score_set = {'S2'}
        score_map, score_set = FSA.get_scores(data)
        self.assertEqual(expected_score_set, score_set)
        expected_score_map = {}
        self.assertEqual(expected_score_map, score_map)

    def test_scores_maximal(self):
        data = self.data_maximal
        expected_score_set = {'S5', 'S4', 'S3', 'S2', 'S1'}
        score_map, score_set = FSA.get_scores(data)
        self.assertEqual(expected_score_set, score_set)
        expected_score_map = {'S2': 2, 'S1': 1}
        self.assertEqual(expected_score_map, score_map)

    def test_get_states_per_section(self):
        expected = ['General', 'Data Management', 'Metadata', 'Publication']
        result = FSA.get_states_per_section()
        self.assertEqual(list(result.keys()), expected)
        self.assertEqual(type(result['General']), type([]))

    def test_generate_text(self):
        data_subset = dict(self.data_minimal[:5])
        # Set bools to True
        data = {}
        for k, v in data_subset.items():
            v['state'] = True
            data[k] = v
        result = FSA.generate_text(data)
        expected = [
            {'paragraphs': [['The data contains sensitive information and approval from the Research Ethics Committee will be sought and any recommendations from the committee will be followed through.', '']], 'section': 'General'},
            {'paragraphs': [], 'section': 'Data Management'},
            {'paragraphs': [], 'section': 'Metadata'},
            {'paragraphs': [], 'section': 'Publication'},
        ]
        self.assertEqual(result, expected)

    def test_get_maximal_previous_states(self):
        result = FSA.get_maximal_previous_states('Q6a')
        self.assertEqual(len(result), 16)
