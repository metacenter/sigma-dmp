from django.contrib import admin

from .models import State, Section, Score
from .models import Text, StateRelation
from .models import FSA


class ScoreAdmin(admin.ModelAdmin):
    list_display = ['name', 'fsa']
admin.site.register(Score, ScoreAdmin)


class SectionAdmin(admin.ModelAdmin):
    list_display = ['name', 'fsa']
admin.site.register(Section, SectionAdmin)


class TextInline(admin.StackedInline):
    model = Text


class TextAdmin(admin.ModelAdmin):
    list_display = ['state', 'condition', 'text']
admin.site.register(Text, TextAdmin)


class StateRelationAdmin(admin.ModelAdmin):
    list_display = ['condition', 'prev_state', 'next_state']
admin.site.register(StateRelation, StateRelationAdmin)


class StateAdmin(admin.ModelAdmin):
    list_display = ['name', 'depends', 'fsa', 'input_type']
    list_filter = ['fsa', 'input_type']
    inlines = [TextInline]
admin.site.register(State, StateAdmin)


class FSAAdmin(admin.ModelAdmin):
    list_display = ['name', 'start']
admin.site.register(FSA, FSAAdmin)
